/**
 *
 * @class SetarObjetos
 * @author Gustavo
 * @project Sequencias
 * @date 08/02/2017
 */
package sequencias.View;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.text.NumberFormat;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.text.NumberFormatter;

/**
 * Classe responsável por setar alguns elementos gráficos
 *
 * @author Gustavo
 */
public class SetarObjetos {

    /**
     * Método responsável por setar os botões no panel inferior
     *
     * @param principalView - objeto da classe PrincipalView
     */
    public void setButtonPanel(PrincipalView principalView) {
        principalView.buttonPanel = new JPanel();
        principalView.buttonPanel.setPreferredSize(new Dimension(0, 60));
        principalView.buttonPanel.setBackground(Color.BLUE);

        principalView.btFibonacci = this.addAButton("Fibonacci", principalView.buttonPanel);
        principalView.btFibonacci.addActionListener(principalView);
        principalView.btFibonacci.setBackground(Color.CYAN);
        principalView.btFibonacci.setForeground(Color.BLACK);

        principalView.btRicci = this.addAButton("Ricci", principalView.buttonPanel);
        principalView.btRicci.addActionListener(principalView);
        principalView.btRicci.setBackground(Color.CYAN);
        principalView.btRicci.setForeground(Color.BLACK);

        principalView.btFetuccine = this.addAButton("Fetuccine", principalView.buttonPanel);
        principalView.btFetuccine.addActionListener(principalView);
        principalView.btFetuccine.setBackground(Color.CYAN);
        principalView.btFetuccine.setForeground(Color.BLACK);
    }

    /**
     * * * Função responsável por setar um JButton
     *
     * @param textoBotao - texto do botão
     * @param container - container onde será adicionado o botão
     * @return JButton
     */
    public JButton addAButton(String textoBotao, Container container) {
        JButton botao = new JButton(textoBotao);
        botao.setAlignmentX(Component.CENTER_ALIGNMENT);
        container.add(botao);
        return (botao);
    }
}
