/**
 *
 * @class PrincipalView
 * @author Gustavo
 * @project Sequencias
 * @date 08/02/2017
 */
package sequencias.View;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JLabel;
import javax.swing.JPanel;
import sequencias.Control.PrincipalController;

/**
 * Classe Visão Principal
 *
 * @author Gustavo
 */
public class PrincipalView extends JFrame implements ActionListener {

    public final JPanel basePanel;
    public JPanel buttonPanel, calcularPanel;
    public JButton btFibonacci, btRicci, btFetuccine;
    private final PrincipalController principalController;
    private final JLabel label;

    /**
     * Construtor da classe
     *
     * @param principalController - objeto da classe PrincipalController
     */
    public PrincipalView(PrincipalController principalController) {
        super("Sequências");
        this.principalController = principalController;
        this.setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));

        basePanel = new JPanel();
        basePanel.setLayout(new BorderLayout());

        label = new JLabel("Selecione a sequência nos botões!");
        label.setFont(new java.awt.Font("Tahoma", 1, 14));
        label.setAlignmentX(Component.CENTER_ALIGNMENT);
        label.setBorder(BorderFactory.createEmptyBorder(30, 0, 0, 0));
        this.add(label);

        // Setando os botões
        new SetarObjetos().setButtonPanel(this);

        basePanel.add(buttonPanel, BorderLayout.PAGE_END);
        this.add(basePanel);

        this.setSize(350, 450);
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    /**
     * Exibindo o basePanel
     */
    public void showPanel() {
        basePanel.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }
}
