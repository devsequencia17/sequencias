/**
 *
 * @class PrincipalController
 * @author Gustavo
 * @project Sequencias
 * @date 08/02/2017
 */
package sequencias.Control;

import sequencias.View.PrincipalView;

/**
 * Classe Controle Principal
 *
 * @author Gustavo
 */
public class PrincipalController {

    public PrincipalController() {
        PrincipalView viewPrincipal = new PrincipalView(this);
        viewPrincipal.showPanel();
    }

}
