/**
 *
 * @class IniciarAplicativo
 * @author Gustavo
 * @project Sequencias
 * @date 08/02/2017
 */
package sequencias.Control;

/**
 * Classe Main
 *
 * @author Gustavo
 */
public class IniciarAplicativo {

    public static void main(String[] args) {
        new PrincipalController();
    }
}
